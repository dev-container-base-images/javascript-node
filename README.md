# Description

Custom javascript-node image based on `mcr.microsoft.com/vscode/devcontainers/javascript-node` using buster source lists.
Official image reference https://github.com/microsoft/vscode-dev-containers/tree/v0.120.0/containers/typescript-node-14/
